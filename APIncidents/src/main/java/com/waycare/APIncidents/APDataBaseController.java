package com.waycare.APIncidents;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.waycare.APIncidents.MainConfiguration;

public class APDataBaseController {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");
	
	private static APDataBaseController instance = null;

	protected APDataBaseController() {
		// Exists only to defeat instantiation.
		@SuppressWarnings("resource")
		MongoClient mongoClient = new MongoClient();
		db = mongoClient.getDatabase(cfg.mainDB);

	}

	public static APDataBaseController getInstance() {
		if (instance == null) {
			instance = new APDataBaseController();
		}
		return instance;
	}
	
	// -------------------------- //
	// -- APDataBaseController -- //
	// -------------------------- //
	// (1) Variables
	// (3) Public methods
	// (3.1) exists
	// (3.2) findOne
	// (3.3) insert
	// (3.4) replace
	// (3.5) updateField
	// (3.5) addList
	// (3.5) removeOne
	// (4) Private methods
	// (5) getters/setters

	// (1) Variables
	private final MongoDatabase db;

	// check exists
	public boolean exists(String cl, String documentIdType,
			String documentIdValue) {

		return (db.getCollection(cl)
				.find(new Document(documentIdType, documentIdValue)).first() != null);

	}
	
	public boolean exists(String cl, String documentIdType1,
			String documentIdValue1,String documentIdType2,
			String documentIdValue2) {

		Document doc = new Document(documentIdType1, documentIdValue1).append(documentIdType2, documentIdValue2);
		
		return (db.getCollection(cl)
				.find(doc).first() != null);

	}
	
	public Document findOne(String cl, String documentIdType,
			String documentIdValue){
	
		
		Document doc = db.getCollection(cl).find(new Document(documentIdType, documentIdValue)).first();
		return doc;
		
	}

	// insertNew - store new document
	public void insert(String cl, String objectType, Document objectValue) {

		db.getCollection(cl).insertOne(new Document(objectType, objectValue));

	}
	
	public void insert(String cl, Document document) {

		db.getCollection(cl).insertOne(document);

	}

	// replace - replace existing document (previous document is removed)
	public void replaceAll(String cl, String documentIdType,
			String documentIdValue, Document document) {

		if (exists(cl, documentIdType, documentIdValue))
			remove(cl, documentIdType, documentIdValue);
		insert(cl, document);

	}
	
	// replace - replace existing document (previous document is removed)
	public void replace(String cl, String documentIdType,
			String documentIdValue, String objectType, Document objectValue) {

		db.getCollection(cl).updateOne(
				new Document(documentIdType, documentIdValue),
				new Document("$set",new Document(objectType, objectValue)));

	}

	// updateField - update single field
	public void updateField(String cl, String documentIdType,
			String documentIdValue, String objectType, Document objectValue) {

		db.getCollection(cl).updateOne(
				new Document(documentIdType, documentIdValue),
				new Document("$set", new Document(objectType, objectValue)));

	}

	// add list
	public void addList(String cl, String documentIdType,
			String documentIdValue, String listType, Document objectValue) {
		db.getCollection(cl).updateOne(
				new Document(documentIdType, documentIdValue),
				new Document("$push", new Document(listType, objectValue)));
	}

	// remove
	public void remove(String cl, String documentIdType, String documentIdValue) {

		db.getCollection(cl).deleteMany(
				new Document(documentIdType, documentIdValue));

	}
}
