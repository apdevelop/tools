package com.waycare.APIncidents;

import java.util.Date;

import org.bson.Document;

import com.sun.syndication.feed.module.georss.geometries.Position;
import com.waycare.APIncidents.APTypes.APAccidentResultType;
import com.waycare.APIncidents.APTypes.APIncidentType;

public class APIncidentBean {

	// Incident Document (incidentObj)
	// - crTime: Date
	// - incidentTime: Date
	// - title: String
	// - type: String
	// - description: String
	// - location: {latLngObj}
	// - incidentCode: String
	// - optSection: String
	// - optPredicted: String

	private Date crTime;
	private Date incidentTime;
	private String title;
	private APIncidentType type;
	private String description;
	private APLatLng location;
	private String incidentCode;
	private String section;

	public APIncidentBean(Date crTime, Date incidentTime, String title,
			APIncidentType type, String description, APLatLng location,
			String incidentCode, String section) {
		super();
		this.crTime = crTime;
		this.incidentTime = incidentTime;
		this.title = title;
		this.type = type;
		this.description = description;
		this.location = location;
		this.incidentCode = incidentCode;
		this.section = section;
	}

	public APIncidentBean(Date incidentTime, String title, APIncidentType type,
			String description, APLatLng location, String incidentCode,
			String section) {
		this(new Date(), incidentTime, title, type, description, location,
				incidentCode, section);
	}

	// 511 api
	public APIncidentBean(String title, Date publishedTime, APIncidentType type,
			String description, Position locationPos, String incidentCode) {
		
		this(new Date(), new Date(), title, type, description, new APLatLng(locationPos.getLatitude(),locationPos.getLongitude()),
				incidentCode);

	}
	
	public APIncidentBean(Date crTime, Date incidentTime, String title,
			APIncidentType type, String description, APLatLng location,
			String incidentCode) {
		this.crTime = crTime;
		this.incidentTime = incidentTime;
		this.title = title;
		this.type = type;
		this.description = description;
		this.location = location;
		this.incidentCode = incidentCode;
		this.section = "NONE";
	}

	public APIncidentBean(Date incidentTime, String title, APIncidentType type,
			String description, APLatLng location, String incidentCode) {
		this(new Date(), incidentTime, title, type, description, location,
				incidentCode);
	}

	public Document getIncidentObj() {

		Document incidentDocument = new Document();
		incidentDocument.put("crTime", this.crTime);
		incidentDocument.put("incidentTime", this.incidentTime);
		incidentDocument.put("title", this.title);
		incidentDocument.put("type", this.type.name());
		incidentDocument.put("description", this.description);
		incidentDocument.put("location", this.location.getLatLngObj());
		incidentDocument.put("incidentCode", this.incidentCode);
		incidentDocument.put("section", this.section);
		return incidentDocument;
	}

	public Date getCrTime() {
		return crTime;
	}

	public void setCrTime(Date crTime) {
		this.crTime = crTime;
	}

	public Date getIncidentTime() {
		return incidentTime;
	}

	public void setIncidentTime(Date incidentTime) {
		this.incidentTime = incidentTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public APIncidentType getType() {
		return type;
	}

	public void setType(APIncidentType type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public APLatLng getLocation() {
		return location;
	}

	public void setLocation(APLatLng location) {
		this.location = location;
	}

	public String getIncidentCode() {
		return incidentCode;
	}

	public void setIncidentCode(String incidentCode) {
		this.incidentCode = incidentCode;
	}

	public String getsection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

}
