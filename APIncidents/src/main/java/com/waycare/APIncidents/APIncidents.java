package com.waycare.APIncidents;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class APIncidents {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");
	
	static DateTimeFormatter dtf_h = DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy");
	static ZoneId zone = ZoneId.of("GMT-5");	
	
	public static void main(String[] args){
		
		// get section locations
		if (args[0] == null){
			LOGGER.fatal("need input vectors location for APNetTrainer");
			System.exit(1);
		}

		ZonedDateTime dateTime = ZonedDateTime.now(zone);
		LOGGER.info("starting APFarm, US time: " + dateTime.format(dtf_h));
		cfg.print();	
	
		APIncidentsCore core = new APIncidentsCore(args[0] + "/Sections");
		core.loadFromDir();
		core.start();
		
	}
}
