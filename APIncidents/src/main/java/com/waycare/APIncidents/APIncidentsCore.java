package com.waycare.APIncidents;

import java.io.File;
import java.util.LinkedList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class APIncidentsCore {

	private static final APDataBaseController dbController = APDataBaseController
			.getInstance();

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	private final String sectionsDir;
	private LinkedList<APLocation> sectionLocations;
	
	
	public APIncidentsCore(String sectionsDir){
		this.sectionsDir = sectionsDir;
		this.sectionLocations = new LinkedList<APLocation>();
	}
	
	// initial loading
	public void loadFromDir() {

		File sectionsDirectory = new File(sectionsDir);

		for (String routeName : sectionsDirectory.list()) {
			File routeDir = new File(sectionsDir + "/" + routeName);
			if (routeDir.isDirectory()) {
				for (String sectionName : routeDir.list()) {
					File nextSectionDir = new File(routeDir + "/" + sectionName);
					if (nextSectionDir.isDirectory()) {

						LOGGER.info("Starting load operations for "
								+ sectionName);
						loadSectionLocation(sectionName, nextSectionDir);

					}
				}
			}
		}
		LOGGER.trace("Done loading sections " + sectionLocations.size());

	}	

	public void start(){
		Accidents511Provider accidentsFeed = Accidents511Provider.getInstance();
		accidentsFeed.init(sectionLocations);
		accidentsFeed.registerFeed();
	}
	
	
	
	
	private void loadSectionLocation(String sectionName, File sectionDir) {

		APLocation newSectionLocation = new APLocation(sectionDir.getAbsolutePath());
		LOGGER.trace("Section " + sectionName + " location loaded");
		sectionLocations.add(newSectionLocation);

	}
	
}
