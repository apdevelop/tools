package com.waycare.APIncidents;

import org.bson.Document;

import com.javadocmd.simplelatlng.LatLng;
import com.javadocmd.simplelatlng.LatLngTool;
import com.javadocmd.simplelatlng.util.LengthUnit;

public class APLatLng {

	private LatLng latLng;
	
	public APLatLng(LatLng latLng){
		this.latLng = latLng;
	}
	
	public APLatLng(double lat, double lng){
		this.latLng = new LatLng(lat,lng);
	}
	
	public APLatLng(Document latLngDocument){
		this.latLng = new LatLng(latLngDocument.getDouble("lat"), latLngDocument.getDouble("lng"));
	}
	
	public Document getLatLngObj(){
		
		// LatLng Document (latLng Object)
		// - lat: Double
		// - lng: Double
		
		Document latLngDocument = new Document();
		latLngDocument.put("lat", latLng.getLatitude());
		latLngDocument.put("lng", latLng.getLongitude());
		return latLngDocument;
		
	}

	public double distance(APLatLng other){
		return LatLngTool.distance(this.latLng, other.getLatLng(), LengthUnit.MILE);
	}
	
	public LatLng getLatLng() {
		return latLng;
	}
	
}
