package com.waycare.APIncidents;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.bson.Document;

import au.com.bytecode.opencsv.CSVReader;

import com.waycare.APIncidents.APLatLng;

/*
 * description:
 * hold AP location unit, should support Lat,Lan, road section, closest town, 
 * (all parameters needed for PE, Online weather and online accidents)
 */
public class APLocation {

	// Location document (locationObj)
	// - route: String
	// - mile: double
	// - length: double
	// - closestTown: String
	// - state: String
	// - majorWeatherStation: String
	// - startLoc: {latLngObj}
	// - endLoc: {latLngObj}
	// - routeDescription: [latLngObj...]

	private String route;
	private double mile;
	private double length;
	private String closestTown;
	private String state;
	private String majorWeatherStation;
	private APLatLng startLoc;
	private APLatLng endLoc;
	private LinkedList<APLatLng> routeDescription;

	
	public APLocation(String sectionDir){
		String locationFile = sectionDir + "/location.csv";

		try {
			CSVReader reader = new CSVReader(new FileReader(locationFile));

			String[] locationDescription = reader.readNext();
			this.route = locationDescription[0];
			this.mile = Double.valueOf(locationDescription[1]);
			this.length = Double.valueOf(locationDescription[2]);
			this.closestTown = locationDescription[3];
			this.state = locationDescription[4];
			this.majorWeatherStation = locationDescription[5];

			String[] startLocationStr = reader.readNext();
			this.startLoc = new APLatLng(Double.valueOf(startLocationStr[0]),
					Double.valueOf(startLocationStr[1]));

			String[] endLocationStr = reader.readNext();
			this.endLoc = new APLatLng(Double.valueOf(endLocationStr[0]),
					Double.valueOf(endLocationStr[1]));

			routeDescription = new LinkedList<APLatLng>();
			routeDescription.add(startLoc);
			String[] nextRouteLine;
			while ((nextRouteLine = reader.readNext()) != null) {
				routeDescription.add(new APLatLng(Double
						.valueOf(nextRouteLine[0]), Double
						.valueOf(nextRouteLine[1])));
			}
			routeDescription.add(endLoc);

			reader.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (NullPointerException e) {
			e.printStackTrace();
			System.exit(1);
		}	
	}
	
	
	public APLocation(Document locationDocument) {

		this.route = locationDocument.getString("route");
		this.mile = locationDocument.getDouble("mile");
		this.length = locationDocument.getDouble("length");
		this.closestTown = locationDocument.getString("closestTown");
		this.state = locationDocument.getString("state");
		this.majorWeatherStation = locationDocument
				.getString("majorWeatherStation");
		this.startLoc = new APLatLng((Document) locationDocument.get("startLoc"));
		this.endLoc = new APLatLng((Document) locationDocument.get("endLoc"));

		routeDescription = new LinkedList<APLatLng>();

		@SuppressWarnings("unchecked")
		List<Document> routeDescriptionList = (List<Document>) locationDocument.get("routeDescription");
		
		for (Document routePoint : routeDescriptionList) {
			routeDescription.add(new APLatLng(routePoint));
		}

	}
	

	public String getSection(){
		return route + "_" + String.valueOf((int) mile) + "_" +  String.valueOf((int) length);
	}
	
	
	public boolean contains(APIncidentBean incident){
		
		return ( minDistance(incident.getLocation()) < 0.5 );
		
	}
	
	
	public double minDistance(APLatLng point){
		
		double minDistance = 99999999.0;
		for (APLatLng routePoint : routeDescription){
			if (routePoint.distance(point) < minDistance){
				minDistance = routePoint.distance(point);
			}
		}
		return minDistance;
	}
	
	

	public Document getDBObject() {

		Document locationDocument = new Document();
		locationDocument.put("route", this.route);
		locationDocument.put("mile", this.mile);
		locationDocument.put("length", this.length);
		locationDocument.put("closestTown", this.closestTown);
		locationDocument.put("state", this.state);
		locationDocument.put("majorWeatherStation", this.majorWeatherStation);
		locationDocument.put("startLoc", this.startLoc.getLatLngObj());
		locationDocument.put("endLoc", this.endLoc.getLatLngObj());

		List<Document> routeDescritpionList = new LinkedList<Document>();
		for (APLatLng routePoint : this.routeDescription) {
			routeDescritpionList.add(routePoint.getLatLngObj());
		}
		locationDocument.put("routeDescription", routeDescritpionList);

		return locationDocument;
	}

	public String getRoute() {
		return route;
	}

	public double getMile() {
		return mile;
	}

	public String getClosestTown() {
		return closestTown;
	}

	public String getState() {
		return state;
	}

	public APLatLng getStartLocation() {
		return startLoc;
	}

	public APLatLng getEndLocation() {
		return endLoc;
	}

	public LinkedList<APLatLng> getRouteDescription() {
		return routeDescription;
	}

}
