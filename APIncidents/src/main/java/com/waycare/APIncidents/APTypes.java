package com.waycare.APIncidents;

public class APTypes {

	public enum APSectionStoringType{
		FULL, QUICK
	}
	
	public enum APSectionLoadingType{
		NO_LOAD, LOAD_OVERRIDE, LOAD_NO_OVERRIDE
	}
	
	public enum APSectionType {
		TOLL_ROAD, INTERSTATE_HIGHWAY, STATE_HIGHWAY, COUNTRY_ROAD, CITY_MAIN_ROAD, CITY_ROAD
	}

	public enum APEngineType {
		MEDIUM, HIGH
	}

	public enum APState {
		CLEAR, MEDIUM, HIGH
	}

	public enum APTimeZone {
		EST, CST, MST, PST
	}

	public enum APIncidentType {
		ACCIDENT, ROAD_INCIDENT, ROAD_RESTRICTION
	}

	// engine/section score type:
	// INITIAL - training score
	// ONLINE - real online testing
	public enum APScoreType {
		INITIAL, ONLINE
	}

	// accident result type:
	// NO_PREDICTION_ENGINE
	// ACCIDENT_PREDICTED
	// ACCIDENT_NOT_PREDICTED
	public enum APAccidentResultType {
		NO_PREDICTION_ENGINE, ACCIDENT_PREDICTED, ACCIDENT_NOT_PREDICTED
	}
	
	public enum APWeatherProviderType{
		YAHOO,WU
	}

}
