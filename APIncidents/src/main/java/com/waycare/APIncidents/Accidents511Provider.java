package com.waycare.APIncidents;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.waycare.APIncidents.APTypes.APIncidentType;
import com.waycare.APIncidents.MainConfiguration;
import com.waycare.APIncidents.APDataBaseController;
import com.waycare.APIncidents.APIncidentBean;
import com.waycare.APIncidents.APLocation;
import com.sun.syndication.feed.module.georss.GeoRSSModule;
import com.sun.syndication.feed.module.georss.GeoRSSUtils;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.fetcher.FeedFetcher;
import com.sun.syndication.fetcher.FetcherException;
import com.sun.syndication.fetcher.impl.FeedFetcherCache;
import com.sun.syndication.fetcher.impl.HashMapFeedInfoCache;
import com.sun.syndication.fetcher.impl.HttpURLFeedFetcher;
import com.sun.syndication.io.FeedException;

public class Accidents511Provider{

	private static Accidents511Provider instance = null;

	protected Accidents511Provider() {
		
	}

	public static Accidents511Provider getInstance() {
		if (instance == null) {
			instance = new Accidents511Provider();
		}
		return instance;
	}

	private static final APDataBaseController dbController = APDataBaseController
			.getInstance();
	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager
			.getLogger(Accidents511Provider.class);
	private final static String INCIDENTS_URL_511 = "http://511nj.org/RSS511Service/RSS511Service.svc/rest/rss/RSSActiveIncidents";

	private URL feedUrl;
	private FeedFetcherCache feedInfoCache;
	private FeedFetcher feedFetcher;

	LinkedList<APLocation> sectionLocations;
	
	public void init(LinkedList<APLocation> sectionLocations){
		this.sectionLocations = sectionLocations;
	}
	
	
	public void registerFeed() {

		try {
			feedUrl = new URL(INCIDENTS_URL_511);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.exit(1);
		}

		feedInfoCache = HashMapFeedInfoCache.getInstance();
		feedFetcher = new HttpURLFeedFetcher(feedInfoCache);

		Timer timer = new Timer(false);
		timer.scheduleAtFixedRate(new AccidentFetcher(), 0,
				cfg.pollingRateMilis_incidents);

	}


	//
	private class AccidentFetcher extends TimerTask {

		@Override
		public void run() {

			try {

				SyndFeed feed = feedFetcher.retrieveFeed(feedUrl);

				// parse feed into entries
				List<SyndEntry> entries = feed.getEntries();
				Iterator<SyndEntry> itr = entries.iterator();

				while (itr.hasNext()) {
					SyndEntry entry = itr.next();
					GeoRSSModule geoEntry = GeoRSSUtils.getGeoRSS(entry);

					APIncidentBean newIncident = new APIncidentBean(
							entry.getTitle(),
							entry.getPublishedDate(),
							getIncidentType(entry.getTitle()),
							entry.getDescription().getValue(),
							geoEntry.getPosition(),
							"511###");
							
					// check if exists in database:
					// TODO: research other implementations to solve:
					// big incidents data sets
					// false positive on similar incidents
					if (!dbController.exists(cfg.incidentsCl, 
							"title", newIncident.getTitle(), "description", newIncident.getDescription())){

						// check if incidents are in sections:
						for(APLocation sectionLocation : sectionLocations){
							
							if( sectionLocation.contains(newIncident) ){
								newIncident.setSection(sectionLocation.getSection());
							}
							
						}
						LOGGER.info("Setting incident" + newIncident.getTitle());
						dbController.insert(cfg.incidentsCl, newIncident.getIncidentObj());
					}
					
					

				}

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (FeedException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (FetcherException e) {
				e.printStackTrace();
				System.exit(1);
			}

		}

		private APIncidentType getIncidentType(String incidentTitle) {

			String[] incidentTypeArr = incidentTitle.split(":");
			String incidentType = incidentTypeArr[1].replaceFirst(" ", "");
			if (incidentType.equals("Accident")
					|| incidentType.contains("Accident")
					|| incidentType.contains("accident")) {

				return APIncidentType.ACCIDENT;

			} else if (incidentType.contains("restriction")
					|| incidentType.contains("Restriction")) {

				return APIncidentType.ROAD_RESTRICTION;

			} else {

				return APIncidentType.ROAD_INCIDENT;

			}
		}

	}

}
