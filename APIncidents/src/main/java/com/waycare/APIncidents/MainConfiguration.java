package com.waycare.APIncidents;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.waycare.APIncidents.APTypes.APSectionLoadingType;

public class MainConfiguration {

	private static MainConfiguration instance = null;

	protected MainConfiguration() {
		// Exists only to defeat instantiation.
	}

	public static MainConfiguration getInstance() {
		if (instance == null) {
			instance = new MainConfiguration();
		}
		return instance;
	}
	
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");
	
	// mongoDB name:
	public String mainDB = "main0";
	
	public String incidentsCl = "IncidentsAll_nj";

	public boolean useFixedIntervals = true;
	private int pollingRateMinutes_incidents  = 5;
	public int pollingRateMilis_incidents = pollingRateMinutes_incidents * 60 * 1000;

	public String zoneId = "America/New_York";
	
	public void print(){
		LOGGER.info("Configuration parameters for run:");
		
		LOGGER.info("Using data base: " + mainDB);
		LOGGER.info("Using collections: " + incidentsCl);
		LOGGER.info("fix intervals: " + useFixedIntervals);
		LOGGER.info("polling rate incidents " + pollingRateMinutes_incidents + " Min.");
	}
	
}
