package com.waycare.APStat;

import java.io.FileWriter;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;

import com.waycare.APStat.APType.APAdjSectionType;

public class APStat {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	public static void main(String[] args) {

		if (args[0] == null) {
			LOGGER.fatal("need section location from APNetTrainer for APStat");
			System.exit(1);
		}

		String sectionsDir = args[0] + "/Sections";
		APStatCore stats = new APStatCore(sectionsDir);
		String[] resultDescription = stats.getResultsDescription();
		String[] resultMain = stats.calcStatsMainSection();
		String[] resultPrev = stats
				.calcStatsAdjSection(APAdjSectionType.PREV_SECTION);
		String[] resultPost = stats
				.calcStatsAdjSection(APAdjSectionType.POST_SECTION);

		try {
			CSVWriter writer = new CSVWriter(new FileWriter(
					cfg.sectionsStatOutputDir + "/" + stats.getOutputFile()),
					',');
			writer.writeNext(resultDescription);
			writer.writeNext(resultMain);
			writer.writeNext(resultPrev);
			writer.writeNext(resultPost);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LOGGER.info("Done stat");
	}
}
