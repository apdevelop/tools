package com.waycare.APStat;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.waycare.APStat.APType.APAdjSectionType;
import com.waycare.APStat.APType.APPredictionResultType;

import au.com.bytecode.opencsv.CSVReader;

public class APStatCore {

	private static MainConfiguration cfg = MainConfiguration.getInstance();
	private static final Logger LOGGER = LogManager.getLogger("GLOBAL");

	private LinkedList<APPredictionResultType> sectionResults;
	private boolean prevSectionExists = false;
	private LinkedList<APPredictionResultType> prevSectionResults;
	private boolean postSectionExists = false;
	private LinkedList<APPredictionResultType> postSectionResults;

	private String outputFile = "";
	private String prevSection;
	private String postSection;
	private String crossSection;

	public APStatCore(String sectionsDir) {

		String[] sectionArr = cfg.sectionId.split("_");
		
		String route = sectionArr[0];
		String mile = sectionArr[1];
		String length = sectionArr[2];

		String sectionDir = sectionsDir + "/" + route + "/" + cfg.sectionId;
		String descriptionFile = sectionDir + "/description.csv";

		// compare all results and store

		// parse description and get adj sections

		String sectionResultFile = sectionDir + "/MEDIUM/predictionResults.csv";
		String prevSectionResultFile = "";
		String postSectionResultFile = "";

		try {
			CSVReader reader = new CSVReader(new FileReader(descriptionFile));
			String[] nextLine;
			if ((nextLine = reader.readNext()) != null) {
				prevSection = nextLine[2];
				postSection = nextLine[3];
				if (!prevSection.equals("none")) {
					prevSectionResultFile = sectionsDir + "/" + route + "/"
							+ prevSection + "/MEDIUM/predictionResults.csv";
					prevSectionExists = true;
				}
				if (!postSection.equals("none")) {
					postSectionResultFile = sectionsDir + "/" + route + "/"
							+ postSection + "/MEDIUM/predictionResults.csv";
					postSectionExists = true;
				}
				// TODO: consider additional cross
				// sections, implement cross section logic
				crossSection = nextLine[4];

			} else {
				LOGGER.fatal("currapted description file for "
						+ descriptionFile);
				System.exit(1);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		// parse and store results from all sections MEDIUM engine (TODO: think
		// of what to do with high engine)
		// TODO: currently all result have same size array.
		sectionResults = getResults(sectionResultFile);
		if (prevSectionExists)
			prevSectionResults = getResults(prevSectionResultFile);
		if (postSectionExists)
			postSectionResults = getResults(postSectionResultFile);

		this.outputFile = route + "_" + mile + "_" + length + "_results.csv";
	}

	public String[] getResultsDescription() {
		return new String[]{cfg.sectionId,prevSection,postSection};
	}

	public String getOutputFile() {
		return outputFile;
	}

	public String[] calcStatsAdjSection(APAdjSectionType adjSectionType) {

		LinkedList<APPredictionResultType> adjSectionResults = new LinkedList<APPredictionResultType>();

		int commonEvents = 0;
		int commonNoEvents = 0;
		int differEvents = 0;
		int differNoEvents = 0;

		switch (adjSectionType) {
		case PREV_SECTION:

			if (!prevSectionExists)
				return new String[] {};

			adjSectionResults = prevSectionResults;
			break;

		case POST_SECTION:

			if (!postSectionExists)
				return new String[] {};

			adjSectionResults = postSectionResults;
			break;

		default:
			LOGGER.fatal("ilegal adj section type " + adjSectionType.name());
			System.exit(1);
		}

		if (adjSectionResults.size() != sectionResults.size()) {
			LOGGER.fatal("adj section (" + adjSectionResults.size()
					+ ") and main section (" + sectionResults.size()
					+ ") results list length differ, currentlly not supported");
			System.exit(1);
		}

		for (int i = 0; i < sectionResults.size(); i++) {
			APPredictionResultType mainSectionResult = sectionResults.get(i);
			APPredictionResultType adjSectionResult = adjSectionResults.get(i);
			boolean common = false;
			boolean noCommon = false;

			if (((mainSectionResult == APPredictionResultType.NO_EVENT_FAIL) || (mainSectionResult == APPredictionResultType.EVENT_SUCCESS))
					&& ((adjSectionResult == APPredictionResultType.NO_EVENT_FAIL) || (adjSectionResult == APPredictionResultType.EVENT_SUCCESS))

			) {
				common = true;
				commonEvents++;
			}

			if (((mainSectionResult == APPredictionResultType.NO_EVENT_SUCCESS) || (mainSectionResult == APPredictionResultType.EVENT_FAIL))
					&& ((adjSectionResult == APPredictionResultType.NO_EVENT_SUCCESS) || (adjSectionResult == APPredictionResultType.EVENT_FAIL))

			) {
				noCommon = true;
				commonNoEvents++;
			}

			if (((mainSectionResult == APPredictionResultType.NO_EVENT_FAIL) || (mainSectionResult == APPredictionResultType.EVENT_SUCCESS))
					&& !common

			) {
				differEvents++;
			}

			if (((mainSectionResult == APPredictionResultType.NO_EVENT_SUCCESS) || (mainSectionResult == APPredictionResultType.EVENT_FAIL))
					&& !noCommon

			) {
				differNoEvents++;
			}

		}

		LOGGER.info(commonEvents);
		LOGGER.info(commonNoEvents);
		LOGGER.info(differEvents);
		LOGGER.info(differNoEvents);
		String[] results = { String.valueOf(commonEvents),
				String.valueOf(commonNoEvents), String.valueOf(differEvents),
				String.valueOf(differNoEvents) };

		return results;

	}

	public String[] calcStatsMainSection() {

		int totalTicks = sectionResults.size();
		int totalEvents = 0;
		int totalNoEvents = 0;
		int noEventSuccess = 0;
		int noEventFail = 0;
		int eventSuccess = 0;
		int eventFail = 0;

		for (APPredictionResultType nextResult : sectionResults) {
			switch (nextResult) {
			case NO_EVENT_SUCCESS:
				totalNoEvents++;
				noEventSuccess++;
				break;
			case NO_EVENT_FAIL:
				totalEvents++;
				noEventFail++;
				break;
			case EVENT_SUCCESS:
				totalEvents++;
				eventSuccess++;
				break;
			case EVENT_FAIL:
				totalNoEvents++;
				eventFail++;
				break;
			default:
				LOGGER.fatal("ilegal prediction result type "
						+ nextResult.name());
				System.exit(1);
			}
		}
		String[] results = { String.valueOf(totalTicks),
				String.valueOf(totalEvents), String.valueOf(totalNoEvents),
				String.valueOf(noEventSuccess), String.valueOf(noEventFail),
				String.valueOf(eventSuccess), String.valueOf(eventFail) };
		LOGGER.info("totalTicks :" + totalTicks);
		LOGGER.info("totalEvents :" + totalEvents);
		LOGGER.info("totalNoEvents :" + totalNoEvents);
		LOGGER.info("noEventSuccess :" + noEventSuccess);
		LOGGER.info("noEventFail :" + noEventFail);
		LOGGER.info("eventSuccess :" + eventSuccess);
		LOGGER.info("eventFail :" + eventFail);

		return results;

	}

	private LinkedList<APPredictionResultType> getResults(String resultFile) {

		LinkedList<APPredictionResultType> results = new LinkedList<APPredictionResultType>();
		try {
			CSVReader reader = new CSVReader(new FileReader(resultFile));
			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				results.add(APPredictionResultType.forInt(Integer
						.valueOf(nextLine[1])));
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		return results;
	}

}
