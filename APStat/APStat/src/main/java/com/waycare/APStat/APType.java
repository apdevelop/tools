package com.waycare.APStat;

public class APType {
	public enum APPredictionResultType{
		NO_EVENT_SUCCESS(0), NO_EVENT_FAIL(1), EVENT_SUCCESS(2), EVENT_FAIL(3);
		
		private final int mask;

		private APPredictionResultType(int mask) {
			this.mask = mask;
		}
		
		public static APPredictionResultType forInt(int id) {
	        for (APPredictionResultType  type : values()) {
	            if (type.mask == id) {
	                return type;
	            }
	        }
	        throw new IllegalArgumentException("Invalid Type id: " + id);
	    }
		public int getMask() {
			return mask;
		}
	}
	
	
	public enum APAdjSectionType{
		PREV_SECTION,POST_SECTION,CROSS_SECTION
	}
}
