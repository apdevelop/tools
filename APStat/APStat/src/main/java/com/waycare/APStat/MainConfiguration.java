package com.waycare.APStat;

public class MainConfiguration {

	private static MainConfiguration instance = null;

	protected MainConfiguration() {
		// Exists only to defeat instantiation.
	}

	public static MainConfiguration getInstance() {
		if (instance == null) {
			instance = new MainConfiguration();
		}
		return instance;
	}
	
	
	public String sectionsStatOutputDir = "Res/sectionsResults";
	// TODO: add array for multiple  stats
	public String sectionId = "i78_55_6";
	
	
	
}
