package com.waycare.AyalonAccidentHandler;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;

import org.bson.Document;

import au.com.bytecode.opencsv.CSVReader;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

/**
 * Hello world!
 *
 */
public class AyalonAccidentHandler 
{
    public static void main( String[] args ) throws IOException
    {
      
    	//getFromFile();
    	singleEvent();
    	
    }
    

    public static void getFromFile() throws IOException{
    	
    	MongoClient mongoClient = new MongoClient();
    	MongoDatabase db = mongoClient.getDatabase("mainAyalon0");
    	
    	CSVReader reader = new CSVReader(new FileReader("Res/accidents.csv"));
        String [] nextLine;
        while ((nextLine = reader.readNext()) != null) {
        
        	db.getCollection("Incidents").insertOne(getDocument(
        			Integer.valueOf(nextLine[0]),
        			Integer.valueOf(nextLine[1]),
        			Integer.valueOf(nextLine[2]),
        			Integer.valueOf(nextLine[3]),
        			Integer.valueOf(nextLine[4]),
        			Double.valueOf(nextLine[5]),
        			Double.valueOf(nextLine[6]),
        			nextLine[7],
        			nextLine[8],
        			nextLine[9]));
        
        }
        reader.close();
    	
    	
    }
    
    
    public static Document getDocument(int year, int month, int day, int hours, int minutes, double lat, double lng, String description, String section, String result){
    	
    	Document accident = new Document();
    	Date crTime = new Date();
    	Date incidentTime = new Date();
    	incidentTime.setYear(year);
    	incidentTime.setMonth(month);
    	incidentTime.setDate(day);
    	incidentTime.setHours(hours);
    	incidentTime.setMinutes(minutes);
    	
    	
		accident.put("crTime", crTime);
		accident.put("incidentTime", incidentTime);
		accident.put("title", "Accident");
		accident.put("type", "ACCIDENT");
		accident.put("description", description);
		
		Document location = new Document();
		
		location.put("lat",lat);
		location.put("lng",lng);
		
		accident.put("location", location);
		accident.put("incidentCode", "Ayalon");
		accident.put("optSection", section);
		accident.put("optAccidentResultType",result);
		return accident;
    }
    
    
    public static void singleEvent(){
    	MongoClient mongoClient = new MongoClient();
    	MongoDatabase db = mongoClient.getDatabase("mainAyalon0");
    	
    	
    	// accident parameters
    	int year = 2016;
    	int month = 3;
    	int day = 16;
    	int hours = 19;
    	int minutes = 50;
    	
		double lat = 32.124461;
		double lng = 34.810012;
		
		String description = "Light accident";
		
		
		//String section = "NB_MDR_AB";
		//String section = "NB_AB_AR";
		//String section = "NB_AR_GL";
		String section = "SB_RZ_AR";
		//String section = "SB_AR_HO";
		//String section = "SB_HO_MDR";
				
		String result = "ACCIDENT_PREDICTED";
		//String result = "NO_PREDICTION_ENGINE";
		//String result = "ACCIDENT_NOT_PREDICTED";
		
    	// accident Document:
    	Document accident = new Document();
    	Date crTime = new Date();
    	Date incidentTime = new Date();
    	incidentTime.setYear(year);
    	incidentTime.setMonth(month);
    	incidentTime.setDate(day);
    	incidentTime.setHours(hours);
    	incidentTime.setMinutes(minutes);
    	
    	
		accident.put("crTime", crTime);
		accident.put("incidentTime", incidentTime);
		accident.put("title", "Accident");
		accident.put("type", "ACCIDENT");
		accident.put("description", description);
		
		Document location = new Document();
		
		location.put("lat",lat);
		location.put("lng",lng);
		
		accident.put("location", location);
		accident.put("incidentCode", "Ayalon");
		accident.put("optSection", section);
		accident.put("optAccidentResultType",result);
    	
    	db.getCollection("Incidents").insertOne(accident);
    	
    }
}
