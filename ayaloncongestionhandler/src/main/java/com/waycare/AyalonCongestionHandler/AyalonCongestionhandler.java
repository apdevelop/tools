package com.waycare.AyalonCongestionHandler;

/**
 * Clean and fix ayalon congestion (sensors) file, 
 * uses simple strategy of averaging each 5 minutes with all other (2010 - 2015) 
 * 
 *
 */
public class  AyalonCongestionhandler
{
    public static void main( String[] args )
    {
    	
    	AyalonCongestionhandlerCore congestionCore = new AyalonCongestionhandlerCore();
    	congestionCore.start("Res/congestion/NB/","Res/congestion/fix/NB/");
    	congestionCore.start("Res/congestion/SB/","Res/congestion/fix/SB/");
    	
    }
}
