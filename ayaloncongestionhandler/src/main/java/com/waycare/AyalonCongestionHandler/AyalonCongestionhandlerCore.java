package com.waycare.AyalonCongestionHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class AyalonCongestionhandlerCore {

	private DateTimeFormatter dtf_full = DateTimeFormatter.ofPattern("dd-MMM-yy hh.mm.ss.000000000 a");
	private DateTimeFormatter dtf_congestion = DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy");

	private HashMap<String, CongestionBean> allData;
	private HashMap<String, Double> observationAvg;
	private HashMap<String, Integer> observationCnt;
	private HashMap<String, Double> densityAvg;
	private HashMap<String, Integer> densityCnt;
	private HashMap<String, Double> velocityAvg;
	private HashMap<String, Integer> velocityCnt;

	public AyalonCongestionhandlerCore() {

		this.allData = new HashMap<String, CongestionBean>();
		this.observationAvg = new HashMap<String, Double>();
		this.observationCnt = new HashMap<String, Integer>();
		this.densityAvg = new HashMap<String, Double>();
		this.densityCnt = new HashMap<String, Integer>();
		this.velocityAvg = new HashMap<String, Double>();
		this.velocityCnt = new HashMap<String, Integer>();

		init();

	}

	public void start(String congestionDir, String outputDir) {

		File folder = new File(congestionDir);
		for (File fileEntry : folder.listFiles()) {

			if (fileEntry.isDirectory())
				continue;
			
			String congestionFile = fileEntry.getName();
			System.out.println("Start fixing sensor " + congestionFile);
			String sensorID = congestionFile.split("\\.")[0];
			try {
				CSVReader reader = new CSVReader(new FileReader(congestionDir + congestionFile));
				String[] nextLine;
				while ((nextLine = reader.readNext()) != null) {
					
					String timeStr = nextLine[0];
					String timeWithLowerCase = timeStr.substring(0,4) + timeStr.substring(4, 7).toLowerCase() + timeStr.substring(7);
					
					LocalDateTime time = LocalDateTime.parse(timeWithLowerCase, dtf_full);

					CongestionBean congestion = new CongestionBean(nextLine[3], nextLine[2], nextLine[5], nextLine[6]);
					allData.put(time.format(dtf_congestion), congestion);
					
					// calculate averages
					if (congestion.getStatus()) {

						if (congestion.getStatusArr()[0]) {
							// observation:
							double observationCurAvg = 0.0;
							try{
								observationCurAvg = this.observationAvg.get(time.withYear(2016).format(dtf_congestion));
							} catch (Exception E){
								E.printStackTrace();
								System.out.println(time.withYear(2016).format(dtf_congestion));
								System.exit(1);
							}
							int observationCurCnt = this.observationCnt.get(time.withYear(2016).format(dtf_congestion));

							double observationNewAvarage = (observationCurAvg * observationCurCnt
									+ congestion.getObservation()) / (observationCurCnt + 1);
							this.observationAvg.put(time.withYear(2016).format(dtf_congestion), observationNewAvarage);
							this.observationCnt.put(time.withYear(2016).format(dtf_congestion),
									this.observationCnt.get(time.withYear(2016).format(dtf_congestion)) + 1);
						}

						if (congestion.getStatusArr()[1]) {
							// density:
							double densityCurAvg = this.densityAvg.get(time.withYear(2016).format(dtf_congestion));
							int densityCurCnt = this.densityCnt.get(time.withYear(2016).format(dtf_congestion));

							double densityNewAvarage = (densityCurAvg * densityCurCnt + congestion.getDensity())
									/ (densityCurCnt + 1);
							this.densityAvg.put(time.withYear(2016).format(dtf_congestion), densityNewAvarage);
							this.densityCnt.put(time.withYear(2016).format(dtf_congestion),
									this.densityCnt.get(time.withYear(2016).format(dtf_congestion)) + 1);
						}

						if (congestion.getStatusArr()[2]) {
							// velocity:
							double velocityCurAvg = this.velocityAvg.get(time.withYear(2016).format(dtf_congestion));
							int velocityCurCnt = this.velocityCnt.get(time.withYear(2016).format(dtf_congestion));

							double velocityNewAvarage = (velocityCurAvg * velocityCurCnt + congestion.getVelocity())
									/ (velocityCurCnt + 1);
							this.velocityAvg.put(time.withYear(2016).format(dtf_congestion), velocityNewAvarage);
							this.velocityCnt.put(time.withYear(2016).format(dtf_congestion),
									this.velocityCnt.get(time.withYear(2016).format(dtf_congestion)) + 1);
						}

					}

				}
				reader.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}

			
			
			fix(sensorID, outputDir, "00:00 01/01/2010", "00:00 01/01/2016");
			write(congestionDir + congestionFile, sensorID, outputDir + congestionFile, congestionDir, "00:00 01/01/2010", "00:00 01/01/2016");
			System.out.println("Done fixing " + congestionFile);
		}

	}

	private void init() {

		// create a one year (16) 5 minutes hash that accumulate avg's
		LocalDateTime from = LocalDateTime.parse("00:00 01/01/2016", dtf_congestion);
		LocalDateTime to = LocalDateTime.parse("00:00 01/01/2017", dtf_congestion);

		while (from.isBefore(to)) {

			observationAvg.put(from.format(dtf_congestion), 0.0);
			observationCnt.put(from.format(dtf_congestion), 0);
			densityAvg.put(from.format(dtf_congestion), 0.0);
			densityCnt.put(from.format(dtf_congestion), 0);
			velocityAvg.put(from.format(dtf_congestion), 0.0);
			velocityCnt.put(from.format(dtf_congestion), 0);

			from = from.plusMinutes(5);

		}

	}

	private void fix(String sensorID, String dir,  String fromStr, String toStr) {

		int fixCnt = 0;
		int rowCnt = 0;
		LocalDateTime from = LocalDateTime.parse(fromStr, dtf_congestion);
		LocalDateTime to = LocalDateTime.parse(toStr, dtf_congestion);

		while (from.isBefore(to)) {
			rowCnt++;
			if ( allData.get(from.format(dtf_congestion)) == null ){
			
				
				CongestionBean congestion = new CongestionBean(true,
						observationAvg.get(from.withYear(2016).format(dtf_congestion)),
						densityAvg.get(from.withYear(2016).format(dtf_congestion)),
						velocityAvg.get(from.withYear(2016).format(dtf_congestion)));
				
				allData.put( from.format(dtf_congestion),  congestion);
				fixCnt++;
			
			}
			
			boolean status = allData.get(from.format(dtf_congestion)).getStatus();
			Boolean[] statusArr = allData.get(from.format(dtf_congestion)).getStatusArr();
			if (!status) {

				// fix bean
				allData.get(from.format(dtf_congestion))
						.setObservation(observationAvg.get(from.withYear(2016).format(dtf_congestion)));
				allData.get(from.format(dtf_congestion)).setDensity(densityAvg.get(from.withYear(2016).format(dtf_congestion)));
				allData.get(from.format(dtf_congestion)).setVelocity(velocityAvg.get(from.withYear(2016).format(dtf_congestion)));
				allData.get(from.format(dtf_congestion)).setStatus(true);
				allData.get(from.format(dtf_congestion)).setStatusArr(new Boolean[] { true, true, true });
				fixCnt++;

			}

			if (!statusArr[0]) {

				allData.get(from.format(dtf_congestion))
						.setObservation(observationAvg.get(from.withYear(2016).format(dtf_congestion)));
				allData.get(from.format(dtf_congestion)).setStatusArr(new Boolean[] { true, true, true });

			}

			if (!statusArr[1]) {

				allData.get(from.format(dtf_congestion)).setDensity(densityAvg.get(from.withYear(2016).format(dtf_congestion)));
				allData.get(from.format(dtf_congestion)).setStatusArr(new Boolean[] { true, true, true });

			}

			if (!statusArr[2]) {

				allData.get(from.format(dtf_congestion)).setVelocity(velocityAvg.get(from.withYear(2016).format(dtf_congestion)));
				allData.get(from.format(dtf_congestion)).setStatusArr(new Boolean[] { true, true, true });

			}

			from = from.plusMinutes(5);

		}
		try {
			
			String sensorQualityStr = sensorID + " " + fixCnt + "/"+ rowCnt + " " +  Double.valueOf(fixCnt)/Double.valueOf(rowCnt) + "\n";
		    Files.write(Paths.get(dir + "sensorQuality"), sensorQualityStr.getBytes(), StandardOpenOption.APPEND);
		    System.out.println(sensorQualityStr);
		
		}catch (IOException e) {
			
			e.printStackTrace();
			System.exit(1);
			
		}
	}

	public void write(String sourceFile, String sensorID, String targetFile, String dir, String fromStr, String toStr) {

		try {
			CSVWriter writer = new CSVWriter(new FileWriter(targetFile));

			LocalDateTime from = LocalDateTime.parse(fromStr, dtf_congestion);
			LocalDateTime to = LocalDateTime.parse(toStr, dtf_congestion);

			while (from.isBefore(to)) {

				String[] nextLine = allData.get(from.format(dtf_congestion)).getLine(from.format(dtf_congestion));
				writer.writeNext(nextLine);
				from = from.plusMinutes(5);

			}
			writer.close();
			String mvFile = dir + "done/" + sensorID + ".csv";
			try{
				Files.move(Paths.get(sourceFile), Paths.get(mvFile) , StandardCopyOption.REPLACE_EXISTING);
			} catch (Exception e){
				e.printStackTrace();
				System.exit(1);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
