package com.waycare.AyalonCongestionHandler;

public class CongestionBean {

	private boolean status;
	private Boolean[] statusArr = new Boolean[3];
	private double observation;
	private double density;
	private double velocity;
	
	public CongestionBean(String status, String observation, String density, String velocity){
		
		// check if sensor read is OK
		
		// check status OK
		if ( !status.trim().equals("OK") ){
			
			this.status = false;
			this.observation = 0.0;
			statusArr[0] = false;
			this.density = 0.0;
			statusArr[1] = false;
			this.velocity = 0.0;
			statusArr[2] = false;
			
		} else {
			
			statusArr = new Boolean[]{true, true, true};
			
			if ( observation.equals("") || observation.equals("0") ){
				
				this.observation = 0.0;
				this.statusArr[0] = false;
				
			} else {
				
				this.observation = Double.valueOf(observation);
				this.statusArr[1] = true;
				
			}
			
			if ( density.equals("") ){
				
				this.density = 0.0;
				this.statusArr[1] = false;
				
			} else {
				
				this.density = Double.valueOf(density);
				this.statusArr[1] = true;
				
			}
			
			if ( velocity.equals("") || velocity.equals("0") ){
				
				this.velocity = 0.0;
				this.statusArr[2] = false;
			
			} else {
				
				this.velocity = Double.valueOf(velocity);
				this.statusArr[2] = true;
				
			}
		
			int cnt = 0;
			for (int i = 0; i < 3; i++)
				cnt = (this.statusArr[i]) ? cnt + 1 : cnt; 
			
			if (cnt < 2){
				
				// 2 or more failed samples cause bean to fail
				this.status = false;
				this.statusArr = new Boolean[]{false,false,false};
			
			} else {
				
				this.status = true;
				
			}
			
		}
		
	}
	
	public CongestionBean(boolean status, double observation, double density, double velocity){
		
		this.status = status;
		this.observation = observation;
		this.density = density;
		this.velocity = velocity;
		this.statusArr = new Boolean[]{true,true,true};
		
	}

	
	public Boolean[] getStatusArr() {
		return statusArr;
	}

	public void setStatusArr(Boolean[] statusArr) {
		this.statusArr = statusArr;
	}

	public double getObservation() {
		return observation;
	}

	public void setObservation(double observation) {
		this.observation = observation;
	}

	public double getDensity() {
		return density;
	}

	public void setDensity(double density) {
		this.density = density;
	}

	public double getVelocity() {
		return velocity;
	}

	public void setVelocity(double velocity) {
		this.velocity = velocity;
	}

	public boolean getStatus(){
		return this.status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public String[] getLine(String time){
		
		String status = (this.status) ? "OK" : "NOT_OK";
		String observation = String.valueOf(this.observation);
		String density = String.valueOf(this.density);
		String velocity = String.valueOf(this.velocity);
		
		return new String[]{time,status,observation,density,velocity};
		
	}
	
}
